package com;


public class Main {
    public static void main(String[] args) {
        Human man = new Man();

        man.greetPet();
        Human man2 = new Woman();
        man2.greetPet();

        Pet dog = new Dog();
        dog.foul();
        Pet rob = new RoboCat();
        rob.foul();
        System.out.println();

        Pet pe = new Pet(3) {
            @Override
            public void respond() {

            }

            @Override
            public void foul() {

            }
        };

    }


}