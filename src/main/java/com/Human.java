package com;

import java.util.HashMap;
import java.util.Map;

public abstract class Human {
    String name;
    String surname;
    int year;
    int iq;
    Family family;
    Map<String, Integer> schedule = new HashMap<>();

    public void welcome() {
        System.out.println("Hello," + family.getPet());
    }

    public abstract void greetPet();


    public void feed() {
    }

    ;

    @Override
    public String toString() {
        return "com.Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + schedule +
                '}';
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }

    public Human() {
    }


    public Human(String name, String surname, int year, int iq, Map<String, Integer> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        if (iq <= 100 || iq >= 1) {
            this.iq = iq;
        } else {
            System.out.println("Your number is higher or smaller than 100 and 1,respectively.So,iq level is set 99 by system");
            this.iq = 99;
        }

        this.schedule = schedule;
    }
}
