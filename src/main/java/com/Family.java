package com;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Family {
     private Human mother;
    Human father;
    ArrayList<Human> children = new ArrayList<>();
    Set<Pet> pet = new HashSet<>();

    public Family(Set<Pet> pet) {
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }


    public ArrayList<Human> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPet() {
        return pet;
    }

    public void setPet(Set<Pet> pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +

                '}';
    }

    public Family(Human mother, Human father, ArrayList<Human> children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public void addChild(Human human) {
        int i;
        int n = children.size();
        ArrayList<Human> newChild = new ArrayList<>(n + 1);
        for (i = 0; i < n; i++)
            newChild.add(children.get(i));
        newChild.add(newChild.size(), human);
        this.children = newChild;
    }

    public boolean deleteChild(int index) {
        boolean deleted = false;
        ArrayList<Human> child = new ArrayList<>(children.size() - 1);

        int i = 0;
        try {
            for (int j = 0; j < children.size(); j++) {
                if (j != index) {
                    child.add(i++, children.get(j));
                }
            }
            if (index < children.size()) {
                deleted = true;
            }
            this.children = child;

            return deleted;
        } catch (
                Exception exception) {

            System.out.println("error");
            return false;
        }

    }

    public int countFamily() {

        return 2 + children.size();
    }


}
