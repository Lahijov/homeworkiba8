import com.Family;
import com.Human;
import com.Pet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import java.util.function.BooleanSupplier;

class FamilyTest {
    public Family family;
    private Human father;
    private Human mother;
    private Human deleted_human;
    private Human added_human;

    Pet pet;

    @BeforeEach
    void familyInfo() {
        mother = new Human("Rafig", "Lahijov", 2002) {
            public void greetPet() {

            }
        };

        father = new Human("Samir", "Lahijov", 1891) {
            public void greetPet() {

            }
        };
        family = new Family(mother, father);

        deleted_human = new Human("deleted_human_1", "Deleted", 1882) {
            public void greetPet() {
            }
        };
        family.addChild(deleted_human);
        added_human = new Human("added_human_1", "added", 1252) {
            public void greetPet() {
            }
        };
        family.addChild(added_human);
    }

    @Test
    void testToString() {
        String expected = "Family{" +
                "mother=" + mother +
                ", father=" + father +

                '}';
        assertEquals(family.toString(), expected);
    }

    @Test
    void addChild() {
        family.addChild(added_human);
        assertTrue(family.getChildren().contains(added_human));

    }


    @Test
    void deleteChild() {

        assertTrue(family.deleteChild(0));
        assertTrue(family.getChildren().contains(deleted_human));
    }

    @Test
    void deleteNonExistingChild() {
        assertFalse(family.deleteChild(10));
    }

    @Test
    void countFamily() {
        assertEquals(4, family.countFamily());
    }


}